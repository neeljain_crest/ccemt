﻿using CEMT.WPF.EntityModels;
using CEMT.WPF.Helpers;
using CEMT.WPF.Manager;
using CEMT.WPF.Models;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;
using System.Windows.Input;

namespace CEMT.WPF.ViewModels
{
    internal class TenantsViewModel : BaseViewModel
    {
        public bool isSearch;

        public bool canForceSync;

        public int TotalPages
        {
            get { return _totalPages; }
        }

        private int _totalPages
        {
            get
            {
                if (isSearch)
                {
                    if (FilteredList != null)
                    {
                        return ((FilteredList.Count - 1) / AppConstants.ItemsPerPage) + 1;
                    }
                    else
                        return 1;
                }
                else
                {
                    if (AllTenantsList != null)
                    {
                        return ((AllTenantsList.Count - 1) / AppConstants.ItemsPerPage) + 1;
                    }
                    else
                        return 1;
                }
            }
        }
        public int CurrentPage;

        public int PageNo
        {
            get { return _pageNo; }
            set
            {
                _pageNo = value;
                OnPropertyChanged("PageNo");
            }
        }

        private int _pageNo;

        public string TenantSearch
        {
            get { return _tenantSearch; }
            set
            {
                _tenantSearch = value;
                OnPropertyChanged("TenantSearch");
            }
        }

        private string _tenantSearch;

        public List<TenantView> AllTenantsList;

        public List<TenantView> FilteredList;

        private List<TenantView> _tenantList;

        public List<TenantView> TenantList
        {
            get { return _tenantList; }
            set
            {
                _tenantList = value;
                OnPropertyChanged("TenantList");
                OnPropertyChanged("TotalPages");
            }
        }

        public ICommand PagePrev { get; }
        public ICommand PageNext { get; }
        public ICommand GetTenants { get; }
        public ICommand DoSearch { get; }
        public ICommand ChangePage { get; }

        public TenantsViewModel()
        {
            PagePrev = new RelayCommand(param => GetPreviousPage(), param => PageNo > 1);
            PageNext = new RelayCommand(param => GetNextPage(), param => PageNo < _totalPages);
            GetTenants = new RelayCommand(param => GetTenantsAsync(), param => canForceSync);
            DoSearch = new RelayCommand(param => DoSearchExcecute(), param => TenantList != null && TenantList.Count > 0);
            ChangePage = new RelayCommand(param => GetPage(), param => TenantList != null && TenantList.Count > 0);
            CurrentPage = 1;
            Schedular schedular = new Schedular(GetTenantsAsyncAuto, AppConstants.TenantSyncInterval, GetDiffTime);
            SchedularManager.AddSchedular(schedular);
            PageNo = 1;
        }

        private void GetNextPage()
        {
            if (isSearch)
            {
                PageNo = PageNo + 1;
                TenantList = Paginator.GetPage(FilteredList, PageNo);
            }
            else
            {
                PageNo = PageNo + 1;
                TenantList = Paginator.GetPage(AllTenantsList, PageNo);
            }
        }

        private void GetPreviousPage()
        {
            if (isSearch)
            {
                PageNo = PageNo - 1;
                TenantList = Paginator.GetPage(FilteredList, PageNo);
            }
            else
            {
                PageNo = PageNo - 1;
                TenantList = Paginator.GetPage(AllTenantsList, PageNo);
            }
        }

        private void GetPage()
        {
            if(0 < PageNo && PageNo < TotalPages)
            {
                if (isSearch)
                {
                    TenantList = Paginator.GetPage(FilteredList, PageNo);
                }
                else
                {
                    TenantList = Paginator.GetPage(AllTenantsList, PageNo);
                }
                CurrentPage = PageNo;
            }
            else
            {
                PageNo = CurrentPage;
                if (isSearch)
                {
                    TenantList = Paginator.GetPage(FilteredList, PageNo);
                }
                else
                {
                    TenantList = Paginator.GetPage(AllTenantsList, PageNo);
                }
            }
            
        }

        public int GetDiffTime()
        {
            if (AppConstants.TenantsLastSync == null)
            {
                return 1;
            }
            else
            {
                TimeSpan span = DateTime.UtcNow - AppConstants.TenantsLastSync.ToUniversalTime();

                if (span.TotalSeconds > AppConstants.TenantSyncInterval)
                {
                    return 1;
                }
                else
                {
                    using (DBModel dB = new DBModel())
                    {
                        AllTenantsList = dB.Tenants_View.ToList();
                        TenantList = Paginator.GetPage(AllTenantsList, PageNo);
                    }

                    return (AppConstants.TenantsLastSync.AddSeconds(AppConstants.TenantSyncInterval) - DateTime.Now).Seconds;
                }
            }
        }

        public async void GetTenantsAsync()
        {
            try
            {
                var url = "http://10.50.1.245/api/tenant";

                HttpClientHandler clientHandler = new HttpClientHandler();

                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                using (var client = new HttpClient(clientHandler))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AppConstants.Token);
                    var response = await client.GetAsync(url);

                    using (DBModel dB = new DBModel())
                    {
                        if ((int)response.StatusCode == 200)
                        {
                            var result = await response.Content.ReadAsStringAsync();

                            TenantManager manager = JsonConvert.DeserializeObject(result, typeof(TenantManager)) as TenantManager;

                            //List<TenantDB> TenantListToRemove = dB.Tenants.ToList();
                            //dB.Tenants.RemoveRange(TenantListToRemove);

                            foreach (Tenant tenant in manager.Tenants)
                            {
                                TenantDB tenantDB = new TenantDB()
                                {
                                    TenantID = tenant.Id.ToString(),
                                    Name = tenant.Name,
                                    DataGeography = tenant.DataGeography,
                                    DataRegion = tenant.DataRegion,
                                    BillingType = tenant.BillingType,
                                    PartnerName = tenant.Partner.Name,
                                    OrganizationName = tenant.Organization.Name,
                                    ApiHost = tenant.ApiHost
                                };
                                dB.Tenants.Add(tenantDB);
                            }

                            dB.SaveChanges();

                            AllTenantsList = dB.Tenants_View.ToList();
                            TenantList = Paginator.GetPage(AllTenantsList, PageNo);
                            canForceSync = false;
                        }
                        else
                        {
                            MessageBox.Show("Something Went Wrong!");
                            WriteLogFile.WriteLog(response.StatusCode + " From URL : http://10.50.1.245/api/OAuth/token/");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFile.WriteLog(ex.Message);
            }
        }

        public async void GetTenantsAsyncAuto()
        {
            try
            {
                var url = "http://10.50.1.245/api/tenant";

                HttpClientHandler clientHandler = new HttpClientHandler();

                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                using (var client = new HttpClient(clientHandler))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AppConstants.Token);
                    var response = await client.GetAsync(url);

                    using (DBModel dB = new DBModel())
                    {
                        if ((int)response.StatusCode == 200)
                        {
                            var result = await response.Content.ReadAsStringAsync();

                            TenantManager manager = JsonConvert.DeserializeObject(result, typeof(TenantManager)) as TenantManager;

                            //List<TenantDB> TenantListToRemove = dB.Tenants.ToList();
                            //dB.Tenants.RemoveRange(TenantListToRemove);

                            foreach (Tenant tenant in manager.Tenants)
                            {
                                TenantDB tenantDB = new TenantDB()
                                {
                                    TenantID = tenant.Id.ToString(),
                                    Name = tenant.Name,
                                    DataGeography = tenant.DataGeography,
                                    DataRegion = tenant.DataRegion,
                                    BillingType = tenant.BillingType,
                                    PartnerName = tenant.Partner.Name,
                                    OrganizationName = tenant.Organization.Name,
                                    ApiHost = tenant.ApiHost
                                };
                                dB.Tenants.Add(tenantDB);
                            }

                            AllTenantsList = dB.Tenants_View.ToList();
                            TenantList = Paginator.GetPage(AllTenantsList, PageNo);

                            SettingsDB settingsDB = dB.Settings.Find(AppConstants.ClientId);
                            settingsDB.TenantLastSync = DateTime.Now;
                            dB.Settings.Update(settingsDB);

                            dB.SaveChanges();

                            canForceSync = true;
                        }
                        else
                        {
                            MessageBox.Show("Something Went Wrong!");
                            WriteLogFile.WriteLog(response.StatusCode + " From URL : http://10.50.1.245/api/OAuth/token/");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFile.WriteLog(ex.Message);
            }
        }

        public void DoSearchExcecute()
        {
            if (!string.IsNullOrEmpty(TenantSearch))
            {
                isSearch = true;
                Func<TenantView, bool> condition = item =>
                    {
                        foreach (var property in typeof(TenantView).GetProperties())
                        {
                            if (property.GetValue(item).ToString().ToLower().Contains(TenantSearch.ToLower()))
                            {
                                return true;
                            }
                        }
                        return false;
                    };
                FilteredList = AllTenantsList.Where(condition).ToList();
                PageNo = 1;
                TenantList = Paginator.GetPage(FilteredList, PageNo);
            }
            else
            {
                isSearch = false;
                using (DBModel dB = new DBModel())
                {
                    PageNo = 1;
                    TenantList = Paginator.GetPage(AllTenantsList, PageNo);
                }
            }
        }
    }
}
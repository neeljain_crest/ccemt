﻿using CEMT.WPF.EntityModels;
using CEMT.WPF.Helpers;
using CEMT.WPF.Models;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace CEMT.WPF.ViewModels
{
    internal class SettingsViewModel : BaseViewModel
    {
        private string _clientId;

        private bool _loggedIn;
        

        public bool LoggedIn
        {
            get { return _loggedIn; }
            set { _loggedIn = value; OnPropertyChanged(nameof(LoggedIn)); }
        }

        public string LoggedClient;
        public string ClientId
        {
            get { return _clientId; }

            set { _clientId = value; OnPropertyChanged(nameof(ClientId)); }
        }

        private string _clientSecret;

        public string ClientSecret
        {
            get { return _clientSecret; }

            set { _clientSecret = value; OnPropertyChanged(nameof(ClientSecret)); }
        }

        public ICommand GetTokenCommand { get; }
        public ICommand LogOutCommand { get; }


        public SettingsViewModel()
        {
            checkLoginOrNot();
            ClientId = "001";
            ClientSecret = "Crest@123";
            GetTokenCommand = new RelayCommand(param => GetTokenAsync(), param => CanLogin);
            LogOutCommand = new RelayCommand(param => Logout());

        }

        private bool CanLogin => !string.IsNullOrEmpty(ClientId) && !string.IsNullOrEmpty(ClientSecret);

        public async void GetTokenAsync()
        {
            try
            {
                string clientSecret = Base64Encode(this.ClientSecret);
                var Clientinfo = new User { ClientID = this.ClientId, ClientSecret = clientSecret };

                var json = JsonConvert.SerializeObject(Clientinfo);

                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var url = "http://10.50.1.245/api/OAuth/token/";

                HttpClientHandler clientHandler = new HttpClientHandler();

                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                using (var client = new HttpClient(clientHandler))
                {
                    var response = await client.PostAsync(url, data);

                    if ((int)response.StatusCode == 200)
                    {
                        var result = await response.Content.ReadAsStringAsync();

                        var tokenResponse = JsonConvert.DeserializeObject(result, typeof(OAuthTokenResponse)) as OAuthTokenResponse;
                        AppConstants.Token = tokenResponse.access_token;

                        using(DBModel dB = new DBModel())
                        {
                            SettingsDB newSettings = new SettingsDB()
                            {
                                ClientID = ClientId,
                                TenantLastSync = DateTime.Now,
                                Page = 10,
                                Token = tokenResponse.access_token,
                                RefreshToken = tokenResponse.refresh_token
                            };
                            dB.Settings.Add(newSettings);

                            AppConstants.ItemsPerPage = 10;
                            AppConstants.TenantsLastSync = DateTime.Now;

                            dB.SaveChanges();
                        }

                        SchedularManager.StartSchedular();

                        AppConstants.ClientId = ClientId;
                        LoggedClient = ClientId;
                        LoggedIn = true;
                    }
                    else if ((int)response.StatusCode == 404)
                    {
                        MessageBox.Show("Invalid Credentials");
                    }
                    else
                    {
                        MessageBox.Show("Something Went Wrong!");
                        WriteLogFile.WriteLog(response.StatusCode + " From URL : http://10.50.1.245/api/OAuth/token/");
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFile.WriteLog(ex.Message);
            }
        }

        async void checkLoginOrNot()
        {
            try
            {
                using (DBModel dB = new DBModel())
                {
                    List<SettingsDB> userData = dB.Settings.ToList();

                    if (userData.Count > 0)
                    {
                        SettingsDB settingsDB = userData[0];
                        String token = settingsDB.Token;
                        String refreshToken = settingsDB.RefreshToken;

                        if (token != null && refreshToken != null)
                        {

                            var Clientinfo = new TokenRefreshRequest { Token = token, RefreshToken = refreshToken };

                            var json = JsonConvert.SerializeObject(Clientinfo);

                            var data = new StringContent(json, Encoding.UTF8, "application/json");

                            var url = "http://10.50.1.245/api/oAuth/refresh-token/";

                            HttpClientHandler clientHandler = new HttpClientHandler();

                            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                            using (var client = new HttpClient(clientHandler))
                            {

                                var response = await client.PostAsync(url, data);

                                if ((int)response.StatusCode == 200)
                                {
                                    var result = await response.Content.ReadAsStringAsync();
                                    var tokenResponse = JsonConvert.DeserializeObject(result, typeof(OAuthTokenResponse)) as OAuthTokenResponse;

                                    if (tokenResponse != null)
                                    {
                                        settingsDB.Token = tokenResponse.access_token;
                                        settingsDB.RefreshToken = tokenResponse.refresh_token;
                                        dB.Update(settingsDB);
                                        dB.SaveChanges();
                                        AppConstants.ClientId = settingsDB.ClientID;
                                        AppConstants.Token = tokenResponse.access_token;
                                        AppConstants.TenantsLastSync = settingsDB.TenantLastSync;
                                        AppConstants.ItemsPerPage = settingsDB.Page;
                                        LoggedIn = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        LoggedIn = false;
                    }

                }
            }
            catch (Exception ex)
            {
                WriteLogFile.WriteLog(ex.Message);
            }

            
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        void Logout()
        {
            try
            {
                using (DBModel dB = new DBModel())
                {
                    dB.Settings.RemoveRange(dB.Settings.ToList());
                    dB.SaveChanges();
                    ClientId = "";
                    ClientSecret = "";
                    LoggedIn = false;
                }
            }
            catch (Exception ex)
            {
                WriteLogFile.WriteLog(ex.Message);
            }
        }
    }
}
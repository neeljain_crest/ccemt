﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CEMT.WPF.EntityModels
{
    public class SettingsDB
    {
        [Key]
        public string ClientID { get; set; }
        public DateTime TenantLastSync { get; set; }
        public int Page { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}

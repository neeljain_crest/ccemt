﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEMT.WPF.EntityModels
{
    public class ProductDB
    {
        [Key]
        public int ProductID { get; set; }
        public string Code { get; set; }
        public string Status { get; set; }
        public string Version { get; set; }
    }
}

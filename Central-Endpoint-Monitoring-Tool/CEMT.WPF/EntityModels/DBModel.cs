﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEMT.WPF.EntityModels
{
    public class DBModel : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=10.50.1.245;userid=user1;password=P@$$4VM;database=CCEMT");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TenantView>().HasNoKey();
        }

        public DbSet<TenantDB> Tenants { get; set; }
        public DbSet<TenantView> Tenants_View { get; set; }
        public DbSet<SettingsDB> Settings { get; set; }
        //public DbSet<Alert> Alerts { get; set; }
        //public DbSet<Endpoint> Endpoints { get; set; }
        //public DbSet<Product> Products { get; set; }
    }
}

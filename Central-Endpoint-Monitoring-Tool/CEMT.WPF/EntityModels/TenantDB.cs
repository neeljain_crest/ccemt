﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEMT.WPF.EntityModels
{
    public class TenantDB
    {
        [Key]
        public string TenantID { get; set; }
        public string Name { get; set; }
        public string DataGeography { get; set; }
        public string DataRegion { get; set; }
        public string BillingType { get; set; }
        public string PartnerName { get; set; }
        public string OrganizationName { get; set; }
        public string ApiHost { get; set; }
    }
}

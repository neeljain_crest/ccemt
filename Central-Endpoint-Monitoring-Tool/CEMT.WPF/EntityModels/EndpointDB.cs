﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CEMT.WPF.EntityModels
{
    public class EndpointDB
    {
        [Key]
        public string EndpointID { get; set; }
        public List<ProductDB> AssignedProducts { get; set; }
        public int PersonID { get; set; }
        public string PersonName { get; set; }
        public string DeviceEncryption { get; set; }
        public string DeviceEncryptionStatus { get; set; }
        public string HealthStatus { get; set; }
        public string ThreatsStatus { get; set; }
        public string ServicesStatus { get; set; }
        public bool OsIsServer { get; set; }
        public string OsPlatform { get; set; }
        public string OsName { get; set; }
        public int OsMajorVersion { get; set; }
        public int OsMinorVersion { get; set; }
        public int OsBuild { get; set; }
        public string Hostname { get; set; }
        public string Ipv4Addresses { get; set; }
        public string Ipv6Addresses { get; set; }
        public DateTime LastSeenAt { get; set; }
        public string MacAddress { get; set; }
        public bool TamperProtectionEnabled { get; set; }
        public TenantDB Tenant { get; set; }
        public string Type { get; set; }
    }
}

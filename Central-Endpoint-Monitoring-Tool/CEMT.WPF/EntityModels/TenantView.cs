﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEMT.WPF.EntityModels
{
    public class TenantView
    {
        public string TenantID { get; set; }
        public string Name { get; set; }
        public string DataGeography { get; set; }
        public string PartnerName { get; set; }
        public string OrganizationName { get; set; }
        public string ApiHost { get; set; }
    }
}

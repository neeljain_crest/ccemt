﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CEMT.WPF.EntityModels
{
    public class AlertDB
    {
        [Key]
        public string AlertID { get; set; }

        public List<string> AllowedActions { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public virtual EndpointDB Endpoint { get; set; }
        public int PersonID { get; set; }
        public string PersonName { get; set; }
        public DateTime RaisedAt { get; set; }
        public string Severity { get; set; }
        public virtual TenantDB Tenant { get; set; }
        public string Type { get; set; }
    }
}

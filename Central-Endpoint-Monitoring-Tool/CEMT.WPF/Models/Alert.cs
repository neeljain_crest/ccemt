﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEMT.WPF.Models
{
    public class Alert
    {
        public List<string> AllowedActions;
        public string Category;
        public string Description;
        public Guid Id;
        public Endpoint_Other Endpoint;
        public Person Person;
        public DateTime RaisedAt;
        public string Severity;
        public Tenant_Other Tenant;
        public string Type;
    }
}

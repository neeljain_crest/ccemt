﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEMT.WPF.Models
{
    public class OAuthTokenResponse
    {
        public string access_token { get; set; }
        public string errorCode { get; set; }
        public int expires_in { get; set; }
        public string message { get; set; }
        public string token_type { get; set; }
        public string refresh_token { get; set; }

    }
}

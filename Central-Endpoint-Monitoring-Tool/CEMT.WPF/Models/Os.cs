﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEMT.WPF.Models
{
    public class Os
    {
        public int Build;
        public bool IsServer;
        public int MajorVersion;
        public int MinorVersion;
        public string Name;
        public string Platform;
    }
}

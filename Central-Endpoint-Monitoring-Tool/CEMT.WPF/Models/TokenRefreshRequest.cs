﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEMT.WPF.Models
{
    internal class TokenRefreshRequest
    {
        public String Token { get; set; }

        public String RefreshToken { get; set; }
    }
}
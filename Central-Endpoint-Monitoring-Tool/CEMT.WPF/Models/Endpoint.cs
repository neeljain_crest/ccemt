﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEMT.WPF.Models
{
    public class Endpoint
    {
        public List<Product> AssignedProducts;
        public Person AssociatedPerson;
        public Encryption Encryption;
        public Group Group;
        public Health Health;
        public string Hostname;
        public Guid Id;
        public string Ipv4Addresses;
        public string Ipv6Addresses;
        public DateTime LastSeenAt;
        public string MacAddress;
        public Os Os;
        public bool TamperProtectionEnabled;
        public Tenant_Other Tenant;
        public string Type;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEMT.WPF.Models
{
    public class Tenant
    {
        public Guid Id;
        public string Name;
        public string DataGeography;
        public string DataRegion;
        public string BillingType;
        public Partner Partner;
        public Organization Organization;
        public string ApiHost;
    }
}

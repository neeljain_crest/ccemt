﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEMT.WPF.Models
{
    public class Health
    {
        public string Overall;
        public Services Services;
        public Threats Threats;
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CEMT.WPF.Helpers
{
    public class Paginator
    {
        public static List<T> GetPage<T>(List<T> EntireList, int PageNo)
        {
            return EntireList.Skip((PageNo - 1) * AppConstants.ItemsPerPage).Take(AppConstants.ItemsPerPage).ToList();
        }
    }
}

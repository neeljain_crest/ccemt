﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CEMT.WPF.Helpers
{
    public class Schedular
    {
        Action Execute;
        int AfterIntervals;
        Func<int> GetFirstInterval;

        public Schedular(Action execute, int afterIntervals, Func<int> getFirstInterval)
        {
            this.Execute = execute;
            this.AfterIntervals = afterIntervals;
            this.GetFirstInterval = getFirstInterval;
        }

        internal void Start()
        {
            Timer aTimer;
            aTimer = new Timer();
            aTimer.Interval =  GetFirstInterval() * 1000;
            aTimer.Elapsed += OnTimedEventA;
            aTimer.AutoReset = false;
            aTimer.Enabled = true;
        }

        private void OnTimedEventA(Object source, ElapsedEventArgs e)
        {
            Execute();
            Timer bTimer;
            bTimer = new Timer();
            bTimer.Interval = AfterIntervals * 1000;
            bTimer.Elapsed += OnTimedEventB;
            bTimer.AutoReset = true;
            bTimer.Enabled = true;
        }

        private void OnTimedEventB(Object source, ElapsedEventArgs e)
        {
            Execute();
        }
    }
}

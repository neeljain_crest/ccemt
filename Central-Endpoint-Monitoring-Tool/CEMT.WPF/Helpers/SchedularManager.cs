﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEMT.WPF.Helpers
{
    public class SchedularManager
    {
        public static List<Schedular> schedulars = new List<Schedular>();

        public static void AddSchedular(Schedular schedular)
        {
            schedulars.Add(schedular);
        }

        public static void StartSchedular()
        {
            foreach(Schedular schedular in schedulars)
            {
                schedular.Start();
            }
        }
    }
}

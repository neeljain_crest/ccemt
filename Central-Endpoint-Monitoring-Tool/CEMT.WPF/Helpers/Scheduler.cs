﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CEMT.WPF.Helpers
{
    public class Scheduler
    {
        Action Execute;
        int Seconds;

        public Scheduler(Action execute, int seconds)
        {
            this.Execute = execute;
            this.Seconds = seconds;
        }

        internal void Start()
        {
            Thread backgroundThread = new Thread(new ThreadStart(myMethod));
            backgroundThread.Start();

        }

        internal void myMethod()
        {
            Execute();
            Thread.Sleep(TimeSpan.FromSeconds(Seconds));
            myMethod();
        }
    }
}

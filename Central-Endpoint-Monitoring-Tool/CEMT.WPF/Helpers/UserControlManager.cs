﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CEMT.WPF.Helpers
{
    class UserControlManager : INotifyPropertyChanged
    {
        private UserControl _currentUserControl;

        public event PropertyChangedEventHandler PropertyChanged;

        public List<UserControl> UserControls { get; set; }
        public UserControl CurrentUserControl
        {
            get { return _currentUserControl; }
            set
            {
                _currentUserControl = value;
                OnPropertyChanged(nameof(CurrentUserControl));
            }
        }

        private ICommand _showUserControlCommand;

        public ICommand ShowUserControlCommand
        {
            get
            {
                return _showUserControlCommand ?? (_showUserControlCommand = new RelayCommand(param => ShowUserControlByClass(param)));
            }
        }

        public UserControlManager(List<UserControl> userControls)
        {
            this.UserControls = userControls;
        }

        public UserControl GetUserControlByClass(string className)
        {
            return UserControls.FirstOrDefault(userControl => userControl.GetType().Name == className);
        }

        public void ShowUserControlByClass(object className)
        {
            CurrentUserControl = this.GetUserControlByClass(className.ToString());
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
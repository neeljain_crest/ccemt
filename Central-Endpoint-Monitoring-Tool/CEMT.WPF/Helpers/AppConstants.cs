﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEMT.WPF.Helpers
{
    public class AppConstants
    {
        public static string Token;
        public static DateTime TenantsLastSync;
        public static int TenantSyncInterval = 3600;
        public static int ItemsPerPage = 10;
        public static string ClientId;
    }
}
    
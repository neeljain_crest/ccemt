﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CEMT.WPF.Helpers
{
    internal class WriteLogFile
    {
        public static bool WriteLog(string strMessage)
        {
            try
            {
                FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", System.AppContext.BaseDirectory, "ccemt.log"), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
                objStreamWriter.WriteLine(DateTime.Now + " : Message: " +   strMessage);
                objStreamWriter.Close();
                objFilestream.Close();
                return true;
            }
            catch (Exception ex)
            {
                WriteLog("Log File Itself Has Error..." + " " + ex.Message);
                return false;
            }
        }
    }
}

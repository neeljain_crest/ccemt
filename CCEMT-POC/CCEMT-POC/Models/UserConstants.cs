﻿using System.Collections.Generic;

namespace CCEMT_POC.Models
{
    public class UserConstants
    {
        public static List<UserModel> Users = new List<UserModel>()
        {
            new UserModel() { Username = "jay rathod", EmailAddress = "jay.admin@email.com", Password = "123", GivenName = "jay", Surname = "rathod", Role = "Administrator" },
            new UserModel() { Username = "ansh patel", EmailAddress = "ansh.seller@email.com", Password = "123", GivenName = "ansh", Surname = "patel", Role = "Seller" },
        };
    }
}
